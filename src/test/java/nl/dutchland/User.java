package nl.dutchland;

import java.util.Objects;

class User {
    private final int id;

    static User fromDataBaseById(final int id) {
        try {
            Thread.sleep(100);
            return new User(id);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private User(int id) {
        this.id = id;
    }

    String name() {
        return "Name:" + this.id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
