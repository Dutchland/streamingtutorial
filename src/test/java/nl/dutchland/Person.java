package nl.dutchland;

import java.time.LocalDate;
import java.time.Period;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class Person {
    private static final Map<Integer, Person> PERSON_DATABASE = Map.of(
            1, new Person("Henk", LocalDate.now().minusYears(30)),
            2, new Person("Jantje", LocalDate.now().minusYears(5)),
            3, new Person("Agnes", LocalDate.now().minusYears(74)),
            4, new Person("Anne", LocalDate.now().minusYears(35)),
            5, new Person("Mieke", LocalDate.now().minusYears(3))
    );
    private final String name;
    private final LocalDate birthDay;

    public Person(String name, LocalDate birthDay) {
        this.name = name;
        this.birthDay = birthDay;
    }

    public boolean isAdultOn(LocalDate referenceDate) {
        // Do not depend on static calls like LocalDate.now(). This makes your code hard to test. Let the user provide these kind of values. This makes this method a true 'function'.
        return Period.between(birthDay, referenceDate).getYears() >= 18;
    }

    public static Optional<Person> personFromDatabase(final Person.Id id) {
        try {
            System.out.println("Retrieving Person from Database with id: " + id.id);
            Thread.sleep(100);
            return Optional.ofNullable(PERSON_DATABASE.get(id.id));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }



    public String getName() {
        return name;
    }

    public LocalDate getBirthDay() {
        System.out.println("Retrieving the birthday of an adult: " + isAdultOn(LocalDate.now()));
        return birthDay;
    }

    static class Id {
        private final int id;

        public Id(final int id) {
            this.id = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Id id1 = (Id) o;
            return id == id1.id;
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }
}
