package nl.dutchland;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.temporal.ValueRange;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.*;

class MainTest {
    /*
    INTRO:
    The streaming API (introduced in Java 8) is very powerful tool. It is a way to write declarative code: describe the flow and the WHAT iso the HOW.
    It can make your code shorter, more readable, lazier but also more complex and unreadable if you use it incorrectly. Like every tool, it is not a silver bullet or golden hammer.
     */

    /*
    HOW THIS TUTORIAL WORKS:
    This tutorial consists of topics that contain a small intro and a set of exercises. These exercises have the form of tests that you have to complete, fix, make green ......
    Constraints when fixing/completing the test are usually enforced by the Assert statement. If there are non enforced constraints this is specified.

    The expected result can usually be cleaned from the test name, variable names and/or Assert statement. Example: Stream<Integer> evenNumbersBelow100 tells that the resulting stream should only contain even numbers below 100.
    If sorting the Stream had been a requirement the variable name would have been sortedEventNumbersBelow100.

    Completing an exercises usually involve fixing or completing a single Stream chain.

    If you finished an exercise or get stuck it is recommended to check the SolutionsTest.java. This class contains a possible solution for all exercises and extra information and/or theory about the solution.
     */

    /*
    INFO: creating Streams
    Creating a Stream is the first step when working with Streams:
    - Create Stream
    - Transform Stream
    - Reduce (to other dataType)

    Streams can be created from a lot of other data types: individual values, Collections, Iterables, Optionals, Maps, (Int)Ranges, Random generators, combining other Streams, .......
    In the exercises below you will get a subset of all possible ways. In later modules you will be forced to discover other possible ways to
     */

    @Test
    void streamFromIndividualValues() {
        // Arrange
        int a = 1;
        int b = 2;
        int c = 3;

        // Act
        Stream<Integer> values = Stream.of(a, b, c);
        assertContainsSameValues(List.of(a, b, c), values);

        // The Stream and StreamSupport classed provide a lot of factory methods to create Streams from various sources.
        // Since Stream is an Interface you cannot create one directly. This structure provides the Java JDK (developers) a lot of flexibility when it comes to changing the implementation (for various reasons) without breaking the API. See book 'Effective Java' item 1.
    }

    @Test
    void streamFromCollection() {
        // Arrange
        Collection<Integer> values = List.of(1, 2, 3);

        // Act
        Stream<Integer> stream = values.stream();

        assertContainsSameValues(List.of(1, 2, 3), stream);
        
        // The .stream() method is located on the Collection<T> interface and can thus be called on ALL implementations of the Collection interface. Lists, Sets, Queues and all other 3th party implementations.
    }
    
    @Test
    void streamFromSpliterator() {
        // Arrange
        Spliterator<Integer> spliterator = List.of(1, 2, 3).spliterator();

        // Act
        boolean couldBeCalledParallel = true;
        Stream<Integer> valuesFromSpliterator = StreamSupport.stream(spliterator, couldBeCalledParallel);

        assertContainsSameValues(List.of(1, 2, 3), valuesFromSpliterator);

        // If you have an Iterable, Iterator or Spliterator available this is a possible way to create a Stream. The parallel parameter of StreamSupport.stream will determine if the Stream is a parallel Stream or not.
        // Parallel Steams will be discussed in a later module but here are some considerations:
        // - does your input source work properly when called parallel? In this simple casi of a List of Integers the answer is Yes.
        // - do you really need the Steam to be parallel (usually because of performance)?
        // Not making the Stream parallel is usually the safest bet. Concurrency bugs are hard to find and reproduce.
    }

    /*
    INFO: changing data types.
    Changing the datatype of a Stream is part of the transform part of Streaming. In this case transforming the datatype within the Stream.

    In the module above we have seen how to create Streams. But why would I want to change my data type to a Steam anyway? One answer lys in the powerful .map() function.
    This map function lets you easily change the type of Stream without having mutable variables. E.g. Stream<String> --> Stream<Integer>.

    Using a Stream with map() is usually also shorter and more readable than using a loop statement.
     */

    @Test
    void mapping() {
        // Arrange
        Collection<String> values = List.of("a", "ab", "abc");

        // Act
        Stream<Integer> valueLengths = values.stream()
                .map(value -> value.length());

        assertContainsSameValues(List.of(1, 2, 3), valueLengths);

        // IDE's often offer alternative ways of writing lambdas. For instance to use a method reference iso a lambda. E.g. 'value -> value.length()' --> 'String::length'
        // Choosing one over the other could be personal preference. Consider code length, readability and consistency with other calls in the chain.

        // Now take a look at this functionality with a loop:
        Collection<Integer> result = new ArrayList<>();
        for (String value : values) {
            result.add(value.length());
        }
        // As you can see the code is slightly longer, uses a mutable Collection and uses references to
    }

    @Test
    void mapInSteps() {
        // Arrange
        List<String> values = List.of("a", "ab", "abc");
        // Map the values to if the number of characters in the values is an even number.
        // Use 2 different .map statements AND call methods within the lambda inside the .map.

        // Act
        Stream<Boolean> valueLengthsEvenness = values.stream()
                .map(value -> numberOfCharactersInValue(value))
                .map(valueLength -> isEven(valueLength));

        assertContainsSameValues(List.of(false, true, false), valueLengthsEvenness);

        // For readability or debugging purposes it is often a good idea to split mapping steps into separate .map() calls and put them on separate lines.
        // Although the numberOfCharactersInValue() and isEven() methods are fairly short, extracting them into functions can improve readability because you can specify a function name.
        // Remember..... the Streaming API is a possibility to let the user read the WHAT and hide the HOW (details). In this case behind a private method.
    }

        /*
    INFO: filtering your Stream.
    Filtering of a Stream is part of the transform part of Streaming. In this case you reduce the number of entries in your Stream.

    A .filter() statement is a gate where an entry in the Stream either passes or is removed. Later steps in your chain can use these filters as preconditions
    Example: filtering all zero values from a Stream<Integer> lets a next step safely divide by the entry without a 'divide by zero error'.

    Remember that Streams can become empty when using filters.
     */

    @Test
    void filtering() {
        // Arrange
        List<String> values = List.of("abc", "a", "ab");

        // Act
        Stream<Integer> stringLengthOfAtLeastLength2 = values.stream()
                .map(MainTest::numberOfCharactersInValue)
                .filter(value -> value >= 2);

        assertContainsSameValues(List.of(3, 2), stringLengthOfAtLeastLength2);

        // In this example you see the beginning of real chains. The order of filtering, mapping and other methods can be of great consequence on the readability, debug-ability or performance of your code.
    }

    @Test
    void filteringChallenges() {
        // Arrange
        List<String> houseNumbers = List.of("7", "20a", "123");

        // Act
        Stream<String> houseNumbersOfAtLeastLength3 = houseNumbers.stream()
                .filter(value -> numberOfCharactersInValue(value) > 2);

        assertContainsSameValues(List.of("123"), houseNumbersOfAtLeastLength3);

        // In this exercise you probably experienced a 'problem' in the Streaming API. You cannot access the original houseNumbers after a call.
        // When creating a neat separate .map() and .filter() method you cannot access the housenumber string anymore....

        // Possible solutions:
        // 1. Combine the steps (like I did here). Drawback is that is reduces readability since we do multiple 'things' in 1 statement.
        // 2. Map to a combined entry that also contains the original value. Like a Map.Entry or a newly created class. See below.
        // Benefit is you can create clean chains and the created class can be a good spot to put utility functions you need in your chain. Sometimes this extra class feels a bit unnatural and unnecessary.

        Stream<String> solution2 = houseNumbers.stream()
                .map(HouseNumberEntry::new)
                .filter(entry -> entry.houseNumberLength() > 2)
                .map(entry -> entry.houseNumber);
    }

    private record HouseNumberEntry(String houseNumber) {
        private Integer houseNumberLength() {
            return houseNumber.length();
        }
    }

    @Test
    void filteringCreatesAnEmptyStream() {
        // Arrange
        List<Integer> values = List.of(1, 2, 3);

        // Act: create any filter that will result in an empty Stream
        Stream<Integer> emptyStream = values.stream()
                .filter(e -> e > 10);

        // Assert
        Assertions.assertTrue(emptyStream.findFirst().isEmpty(), "The stream is not empty!");
    }

    @Test
    void filterPerformance() {
        // Arrange
        List<Integer> values = List.of(50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 69, 61, 103, 101);
        long startTimeInMilliseconds = System.currentTimeMillis();

        // Act: Retain values that conform to the A_VERY_EXPENSIVE_FILTER variable and are >=101.
        Stream<Integer> filteredValues = values.stream()
                .filter(value -> value >= 101)
                .filter(A_VERY_EXPENSIVE_FILTER);

        // Assert
        List<Integer> result = filteredValues.collect(toList());
        final long runTimeInMilliseconds = System.currentTimeMillis() - startTimeInMilliseconds;
        System.out.println("Runtime was: " + runTimeInMilliseconds + " in milliseconds");
        Assertions.assertTrue(runTimeInMilliseconds < 2 * 100 + 10, "Find a way to make the filtering more efficient");

        assertContainsSameValues(List.of(103, 101), result.stream());

        // If you found yourself having problems with the time constraint in this exercise you probably filtered on A_VERY_EXPENSIVE_FILTER first.
        // If not. Please try for yourself.

        // The takeaway here is to think about the order of your statements. What steps will be cheap (simple calculations)? What steps are expensive (like DBs and network/IO calls)?
        // Can you find a way to reduce the number of expensive calls? Even if there are no expensive steps, the order of the steps can improve readability.
    }

    private static final Predicate<Integer> A_VERY_EXPENSIVE_FILTER = value -> {
        try {
            Thread.sleep(100);
            return value % 2 == 1;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    };

    /*
    Reducing is the last of 3 steps. Creating a stream --> transforming --> create result.
    A Stream as a result is usually not the end. Passing a stream to different parts of the application is not recommended. One of those reasons becomes apparent in an exercise below.
     */

    @Test
    void reducingToSet() {
        // Arrange
        List<Integer> values = List.of(-1, 0, 1, 2, 3, 4, 4, -3);

        // Act
        Set<Integer> uniquePositiveValues = values.stream()
                .filter(value -> value >= 0)
                .collect(Collectors.toSet());

        // Assert
        assertContainsSameValues(List.of(0, 1, 2, 3, 4), uniquePositiveValues.stream());

        // The collect() method expects a Collector. A lot of default Collectors can be found as static factory methods on the Collectors class. Scroll through this class for inspiration.
        // You can collect to a lot of datatypes including Maps, different types of Collections, String or write a function to reduce your Stream to any type with the .reduce() method.
        // It would be wise to check the existing functionality before writing your own Collectors.

        // Since Java16 the streaming API also contains the commonly used .toList() method directly on the Stream interface.
    }

    @Test
    void reducingStreamMultipleTimes() {
        // Arrange
        Stream<Integer> values = Stream.of(1, 2, 3, 4);

        // In this exercise you must add an assert statement for the

        // Act
        List<Integer> collectedToList = values.toList();
        String reducedToString = values
                .map(Object::toString)
                .collect(joining(","));

        // Assert
        assertContainsSameValues(List.of(1,2,3,4), collectedToList);
        // Create an assert to check the content of the reducedToString method.
    }

    @Test
    void deDuplicate() {
        // Arrange
        List<Integer> values = List.of(1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3);
        long startTimeInMilliseconds = System.currentTimeMillis();

        // Act: Return all unique integers that match A_VERY_EXPENSIVE_FILTER
        Set<Integer> uniqueUnevenIntegers = values.stream()
                .distinct()
                .filter(A_VERY_EXPENSIVE_FILTER)
                .collect(toSet());

        // Assert
        final long runTimeInMilliseconds = System.currentTimeMillis() - startTimeInMilliseconds;
        System.out.println("Runtime was: " + runTimeInMilliseconds + " in milliseconds");
        Assertions.assertTrue(runTimeInMilliseconds < 3 * 100 + 10);

        assertContainsSameValues(List.of(1, 3), uniqueUnevenIntegers.stream());
    }

    @Test
    void reducing2() {
        List<String> values = List.of("Jan", "Anna", "Henk", "Agnes");
        // Provide a String with all names that start with A concatted and seperated with ', '.

        String concattedValuesThatStartWithA = values.stream()
                .filter(value -> value.startsWith("A"))
                .collect(joining(", "));

        Assertions.assertEquals("Anna, Agnes", concattedValuesThatStartWithA);
    }

    @Test
    void reducing3() {
        List<String> values = List.of("Jan", "Anna", "Henk", "Agnes");

        Optional<String> firstValueThatStartsWithA = values.stream()
                .filter(value -> value.startsWith("A"))
                .findFirst();

        Assertions.assertEquals(Optional.of("Anna"), firstValueThatStartsWithA);
    }

    @Test
    void reducing4() {
        Stream<String> values = Stream.of("Jan", "Anna", "Henk", "Agnes");
        // Provide a boolean that tells if there is any name in the stream that starts with an X. You cannot use the .filter method.
        boolean anyNameStartsWithX = values
                .anyMatch(value -> value.startsWith("X"));

        Assertions.assertFalse(anyNameStartsWithX);
        // An empty Stream or a Stream that has been filtered so it contains no more values will always return false.
    }

    @Test
    void reducing5() {
        Stream<Integer> values = Stream.of(1, 2, 3, 4);
        // Calculate the sum of the values using a Stream. You cannot use an IntStream.

        int sum = values
                .reduce(0, Integer::sum);

        Assertions.assertEquals(10, sum);

        // There are multiple reduce methods. You can reduce to any type you want including Collections
    }

    @Test
    void grouping1() {
        Collection<String> names = List.of("Anna", "Bert", "Pascal", "Ger");

        Map<Integer, List<String>> namesByLength = names.stream()
                .collect(groupingBy(String::length));
    }

    @Test
    void reducing6() {
        Map<Integer, String> values = Map.of(
                1, "a",
                2, "b",
                3, "c");

        Map<Integer, String> filteredMap = values.entrySet().stream()
                .filter(e -> e.getKey() > 1)
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Test
    void reducing7() {
        Collection<String> names = List.of("Anna", "Pascal", "Anna", "Ad");
        // Create a map that maps each name to the names length.
        Map<String, Integer> namesToNameLength = names.stream()
                .collect(toMap(Function.identity(), String::length));
    }

    @Test
    void unlimitedIntegers() {
        IntStream unEndingInts = new Random().ints();

        IntStream tenValuesUnder100 = unEndingInts
                .filter(i -> i < 100)
                .limit(10);
    }

    @Test
    void sortedLogEntries() throws URISyntaxException {
        final Path pathToLogCsvFile = Paths.get(this.getClass().getResource("logfile.csv").toURI());
        // Give me all LogEntries from 2021. You cannot use .filter().

        try (Stream<String> logLines = Files.lines(pathToLogCsvFile)) {
            Stream<LogEntry> logEntriesIn2021 = logLines
                    .skip(1) // Skip the header line
                    .map(LogEntry::fromCsvLine)
                    .dropWhile(entry -> entry.year() < 2021)
                    .takeWhile(entry -> entry.year == 2021);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // You should not use .filter() since it will need do consume the entire stream. Because you know the list is ordered you can use dropWhile and takeWhile.
    }

    private record LogEntry(int year, String message) {
        private LogEntry(final int year, final String message) {
            this.year = year;
            this.message = message;
        }

        public static LogEntry fromCsvLine(final String line) {
            String[] split = line.split(",");
            return new LogEntry(Integer.valueOf(split[0]), split[1]);
        }
    }

    @Test
    void flatMapping() {
        List<Character[]> values = List.of(
                new Character[]{'a', 'b', 'c'},
                new Character[]{'d', 'e', 'f'},
                new Character[]{'g', 'h', 'i'});
        Set<Character> vauls = Set.of('a', 'e', 'i', 'o', 'u');
        // Return all vaul characters in the total set.

        Stream<Character> foundVauls = values.stream()
                .flatMap(value -> Arrays.stream(value))
                .filter(character -> vauls.contains(character));

        assertContainsSameValues(List.of('a', 'i', 'e'), foundVauls);
    }

    @Test
    void optionalStream() {
        List<Optional<String>> optionalCollection = List.of(Optional.of("H"), Optional.empty(), Optional.of("i"), Optional.empty());
        // Concat all non empty strings.

        String result = optionalCollection.stream()
                .flatMap(Optional::stream)
                .collect(joining());

        Assertions.assertEquals("Hi", result);

    }

    @Test
    void whenNotToUseAStream() {
        List<Integer> values = List.of(1, 3, 2, 8, 11, 4, 10, 100);
        List<Integer> largestNumbers = List.of(1, 3, 8, 11, 100);

    }

    // There are a few things to think about when constructing your filtering. Readability, performance, debugability.
    // Readability. Since you spend most of your time reading code the readability of your stream is important.
    // - Extract non trivial operations to seperate variables or methods. Even simple methods like isEven(even) can help since you get to name the variable/method.
    //    Naming variables lets the reader focus on the logic and flow (the what) of the algorithm iso the little details of every step (the how).
    //    This fits well in de declarative style of the Streaming api.
    //  - Put every .map .filter .... on a seperate line. This way readers only have to read from top to bottom and not from left to right. This is also benefitial for debugging.
    //  - Readability: Especially when your stream 'train' gets long ....... See if successive filter filter or map map 'wagons' can be merged. The merged wagon should still do 1 thing.
    //      A good check if two wagons can merge is to think of a method name for the merge step. If you have trouble thinking of a simple (non combined) method names. The wagons should not be merged.
    //  - Method reference notation is shorter then lambda notation most of the time. It is not always more readable. Getting to name a variable in lambda notation can be what you need.

    // Keep it simple stupid. Dont be clever, use as much of the existing API as possible and dont write it yourself. People familiar with the API will be able to read your code a lot faster.
    // Lets say you want to collect a list of all Strings from a Stream of byte[]s. .filter(bytes -> containsA(bytes).map(String::new) is faster then the other way around.
    // This will require custom code where you could also use the String.contains() method instead.


    // Performance
    // - Declaring a Stream is lazy. Only when calling the stream code gets executed.
    // - The execution is as lazy as possible. For instance when the result of your algorithm is a single value. Dont collect to a Collection and pick a value from it.
    // Use the anyMatch() (for a boolean), findFirst() (or in a parallelStream the findAny()) case.
    // In most cases all or most 'wagons' only have to executed until an acceptable answer has been found.
    // Be smart about the order you filter. Expensive filters (that require expensive computations, db calls, network calls....) as late as possible. Filters that filter out most values to the front
    // Lets say you have a list of Person (contains address and name) objects. If you want the persons that have an e, a or o in their name AND they have to live in Schiedam....
    // First filter on Schiedam since most people wont live in Schiedam. Then filter on the name thing since most peoples names will have an e, a or o.

    private static int numberOfCharactersInValue(final String value) {
        return value.length();
    }

    private static boolean isEven(final int value) {
        return value % 2 == 0;
    }


    @Test
    void testParallel() {
        Set<Integer> userIds = Set.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        long startTimeInMilliseconds = System.currentTimeMillis();
        // code between here

        Set<User> users = userIds.parallelStream()
                .map(id -> User.fromDataBaseById(id))
                .collect(toSet());
//        and here

        final long runTimeInMilliseconds = System.currentTimeMillis() - startTimeInMilliseconds;
        System.out.println("Runtime was: " + runTimeInMilliseconds + " in milliseconds");
        Assertions.assertTrue(runTimeInMilliseconds < userIds.size() * 100);

        // TODO check if Set contains all users

    }

    @Test
    void combineStreams() {
        Stream<Integer> stream1 = Stream.of(1, 2, 3);
        Stream<Integer> stream2 = Stream.of(4, 5, 6);

        assertSameOrder(Stream.of(1, 2, 3, 4, 5, 6), Stream.concat(stream1, stream2));
    }

    @Test
    void typeSpecificStreams() {
        List<String> values = List.of("abc", "1234", "de");

        IntStream valueLengths = values.stream()
                .mapToInt(String::length);

        Integer sumOfAllLengths = valueLengths.sum();

        Assertions.assertEquals(9, sumOfAllLengths);
        // Streams like DoubleStream IntStream and Longstream have special methods that can only be called on numbers. Sum, average ......
    }


    private <T> void assertContainsSameValues(List<T> expectedValues, Stream<T> stream) {
        List<T> values = stream.collect(toList());
        Assertions.assertEquals(expectedValues.size(), values.size());
        Assertions.assertTrue(values.containsAll(expectedValues), "Stream does not contain all required values:" + expectedValues.toString());
    }

    private <T> void assertContainsSameValues(List<T> expectedValues, List<T> otherList) {
        Assertions.assertEquals(expectedValues.size(), otherList.size());
        Assertions.assertTrue(otherList.containsAll(expectedValues), "Stream does not contain all required values:" + expectedValues.toString());
    }

    private <T> void assertSameOrder(Stream<T> expectedValues, Stream<T> actualValues) {
        Assertions.assertEquals(expectedValues.collect(toList()), actualValues.collect(toList()));
    }


    @Test
    void youngestChildInDenHaag() {
        List<House> houses = List.of(new House1(), new House2(), new House3()); // TODO add houses. 1 in den haag 1 somewhere else.
        // Cannot use parallelstream. Check logs to see what is happening

        //                .filter(House::hasChildren)
        // Is Optional
        Optional<Person> nameOfYoungestChild = houses.stream()
                .filter(House::hasChildren)
                .flatMap(house -> house.residentIds().stream())
                .flatMap(id -> Person.personFromDatabase(id).stream())
                .filter(person -> !person.isAdultOn(LocalDate.now())) // Is optional
//                .sorted(Comparator.comparing(Person::getBirthDay).reversed())
//                .map(Person::getName)
//                .findFirst();
                .max(Comparator.comparing(Person::getBirthDay));
//

        Assertions.assertEquals("Mieke", nameOfYoungestChild.get().getName());
    }
}

//Parallelstream in combinatie met distinqt, sorted
// distinqt vs Collectors.toSet(). Geef me alle unique integers of geef me de som van alle unique integers.

// findFirst vs findAny, anyMatch, allMatch (in combinatie met lege stream)
// gebruik van IntStream/DoubleStream ...... Van normale stream naar Intstream.

// Gebruik van parallelstream na een sorted geeft geen garantie meer over de volgorde.

// Lijst met Huis objecten met een lijst van bewoners per Huis.
// Bewoners hebben een naam en een geboortedatum.
// Geef me de oudste persoon die woont in Den Haag
// Geef met de op 1 na oudste persoon die woont in Den Haag
// Geef me de 3 oudste personen (zonder Collections te gebruiken)

// Infinate stream. uitleg en opdracht om de eerste 1000 even getallen uit een random te halen (gebruik van limit)
