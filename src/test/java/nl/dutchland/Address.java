package nl.dutchland;

public interface Address {
    String city();
    String street();
    int houseNumber();
}
