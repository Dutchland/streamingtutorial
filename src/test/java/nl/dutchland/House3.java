package nl.dutchland;

import java.util.Collection;
import java.util.List;

public class House3 implements House {
    @Override
    public Collection<Person.Id> residentIds() {
        System.out.println("Calling resident ids");
        return List.of(new Person.Id(4),new Person.Id(5));
    }

    @Override
    public Address address() {
        return new Address() {
            @Override
            public String city() {
                return "Den Haag";
            }

            @Override
            public String street() {
                return "Spuimarkt";
            }

            @Override
            public int houseNumber() {
                return 1;
            }
        };
    }

    @Override
    public boolean hasChildren() {
        return true;
    }
}
