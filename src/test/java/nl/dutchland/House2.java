package nl.dutchland;

import java.util.Collection;
import java.util.List;

public class House2 implements House {
    @Override
    public Collection<Person.Id> residentIds() {
        System.out.println("Calling resident ids");
        return List.of(new Person.Id(3));
    }

    @Override
    public Address address() {
        return new Address() {
            @Override
            public String city() {
                return "Den Haag";
            }

            @Override
            public String street() {
                return "Laan van Ypenburg";
            }

            @Override
            public int houseNumber() {
                return 2;
            }
        };
    }

    @Override
    public boolean hasChildren() {
        return false;
    }
}
