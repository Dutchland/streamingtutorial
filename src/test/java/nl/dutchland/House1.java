package nl.dutchland;

import java.util.Collection;
import java.util.List;

public class House1 implements House {
    @Override
    public Collection<Person.Id> residentIds() {
        System.out.println("Calling resident ids");
        return List.of(new Person.Id(1),new Person.Id(2));
    }

    @Override
    public Address address() {
        return new Address() {
            @Override
            public String city() {
                return "Delft";
            }

            @Override
            public String street() {
                return "Oude Langedijk";
            }

            @Override
            public int houseNumber() {
                return 3;
            }
        };
    }

    @Override
    public boolean hasChildren() {
        return true;
    }
}
