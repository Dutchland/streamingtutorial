package nl.dutchland;

import java.util.Collection;

public interface House {
    Collection<Person.Id> residentIds();
    Address address();

    boolean hasChildren();
}
